// const cluster = new couchbase.Cluster("couchbase://127.0.0.1")
// cluster.authenticate("Administrator", "passer@123");
// const N1qlQuery = couchbase.N1qlQuery;
// const bucket = cluster.openBucket("codes");
// bucket.operationTimeout = 5000;

async function run(){
    const data = require('./doc2.json');

    var MongoClient = require('mongodb').MongoClient;
    var url = "mongodb://192.168.99.100:32768"; // CHANGE CONNECTION DATA
    const db = await MongoClient.connect(url)
    var dbo = db.db("codeBD");

    console.log('We are connected in DB');
    const collection =  dbo.collection("codecontent")
    for (const row of data) {
        await insertRow(row, collection);
    }
    db.close();
    console.log('OK')
};

async function insertRow(row, collection, parent) {

    let newData = { Title: row.title, Text: row.value, Type: row.type };
    if(parent){
        newData.parent = parent;
    }

    let id = (await collection.insertOne(newData)).insertedId;
    if(row.children){
        for (const child of row.children) {
            await insertRow(child, collection, id);
        }
    }

}


// ===============
try {

    run();
} catch (error) {
    console.log('FAILED');
    console.log(error)
}