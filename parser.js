
const cluster = new couchbase.Cluster("couchbase://127.0.0.1")
cluster.authenticate("Administrator", "passer@123");
const N1qlQuery = couchbase.N1qlQuery;
const bucket = cluster.openBucket("codes");
bucket.operationTimeout = 5000;

class fileParser {
    
    constructor(markers) {
        this.markers = markers || ['N', 'L', 'T', 'C', 'S', 'P', 'A']
    }

    parser(container = [], data = [], lastPosition) {
        // condition de sorti
        if (!data || data.length == 0) return container;

        const position = this.readLine(data);
        
        if (lastPosition == undefined || position > lastPosition) { // position == lastPosition ||
            //if not root THEN find and read children


            var content = this.findMarquer(data);
            var title = data[content.start];
            // var numItemToRemove = (content.end - 1) - content.start;// positioning the cursor to the next marker
            data.splice(0, content.end)// remove current marker and it's content from data
            var children = [];
            if (position != this.markers.length - 2) {
                children = this.parser([], data, position)// return position of parser in children for all children node
            }

            let n = { title, type: content.type, value: content.value, ...(children.length ? { children } : {}) } // { children: children }
            container.push(n)


            bucket.insert('code::2156', JSON.stringify(container), function (err, result) {
                if (err) {
                  console.log(err)
                } else {
                  console.log(result)
                }
            })


            return this.parser(container, data, lastPosition)
        } else {
            return container;
        }
    }

    readLine(data) {
        var marker = data[0] // position of the fist item in markers
        var res = marker.split(" ");// get only the fist element 
        return this.markers.indexOf(res[0]);
    }

    /**
     * - skip all line until we reach next marker
     * - when we reach a marker, let's find the end of the marker
     * - retourner un object qui decrit le block
     *  - defini le offset
     * @param {*} data
     */
    findMarquer(data) {
        //identifier un marker
        let index = 0;
        let marker_start;
        let marker_end;
        let marker_type;
        let is_marker_start = false;
        let is_marker_end = false;
        for (index; index < data.length; index++) {
            if (!is_marker_start) {
                marker_type = data[index] && this.markers.find(mark => data[index].startsWith(mark));
                if (marker_type) {
                    is_marker_start = true;
                    marker_start = index;
                }
                continue;
            }
            if (!is_marker_end) {
                is_marker_end = data[index] && this.markers.find(mark => data[index].startsWith(mark));
                if (is_marker_end) {
                    marker_end = index;
                }
            }
            if (is_marker_start && is_marker_end) {
                break;
            }
        } // e

        if (is_marker_start) {
            var end = marker_end || data.length;
            var value = this.copyContent(marker_start, end, data);// get content between two markers
            let result = { type: marker_type, start: marker_start, end, value } // check
            return result;
        } else {
            return null;
        }


    }

    //copy content between two markers
    copyContent(marker_start, marker_end, data) {
        var content = "";
        for (var i = marker_start + 1; i < marker_end; i++) {
            content += data[i] + ' ';
        }
        return content;
    }
}

module.exports = fileParser;
