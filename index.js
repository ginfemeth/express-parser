const express = require("express");
const couchbase = require("couchbase");
const bodyParser = require("body-parser");
const path = require("path");

const fs = require('fs');
const pdf = require('pdf-parse');

const app = express();
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(function (req, res, next) {
    res.locals.userValue = null;
    next();
})

const cluster = new couchbase.Cluster("couchbase://127.0.0.1")
cluster.authenticate("Administrator", "passer@123");
const N1qlQuery = couchbase.N1qlQuery;
const bucket = cluster.openBucket("senC");
bucket.operationTimeout = 5000;

// Include my parser class
// var fileParser = require('./parser');

class fileParser {

    constructor(markers) {
        this.markers = markers || ['N', 'L', 'T', 'C', 'S', 'P', 'A']
    }

    async parser(parentId, container = [], data = [], lastPosition) {
        // condition de sorti
        if (!data || data.length == 0) return container;

        const position = this.readLine(data);

        if (lastPosition == undefined || position > lastPosition) { // position == lastPosition ||
            //if not root THEN find and read children

            // var nextIdNumber = bucket.counter(key, 1).codes();
            
            var content = this.findMarquer(data);
            var title = data[content.start];
            // var numItemToRemove = (content.end - 1) - content.start;// positioning the cursor to the next marker
            data.splice(0, content.end)// remove current marker and it's content from data
            
            let n = { title, type: content.type, value: content.value, parentId:parentId } // { children: children }
            // insert N in db
            let insertId = await this.insertItem(n);


            var children = [];
            if (position != this.markers.length - 2) {
                children = await this.parser(insertId, [], data, position)// return position of parser in children for all children node
            }

            if(children.length){
                n.children = children;
            }
            container.push(n)
           


            return this.parser(parentId, container, data, lastPosition)
        } else {
            return container;
        }
    }

    readLine(data) {
        var marker = data[0] // position of the fist item in markers
        var res = marker.split(" ");// get only the fist element
        return this.markers.indexOf(res[0]);
    }

    insertItem(data){
        //insert to database
        console.log("insert called")
        return new Promise( function(resolve, reject){
            var key = "nodeDevguideExampleCounter";
            console.log("promise called", data)
            bucket.counter(key, 1, {initial: 5000}, function(err, res) {
                if (err){
                    reject(err);
                }else{
                    bucket.insert('doc::'+res.value, JSON.stringify(data), function (err, result) {
                        if (err) {
                          console.log(err)
                          reject(err);
                        } else {
                            console.log(result);
                            resolve('doc::'+res.value);
                        }
                    })
                }
            });
        } );
    }



    /**
     * - skip all line until we reach next marker
     * - when we reach a marker, let's find the end of the marker
     * - retourner un object qui decrit le block
     *  - defini le offset
     * @param {*} data
     */
    findMarquer(data) {
        //identifier un marker
        let index = 0;
        let marker_start;
        let marker_end;
        let marker_type;
        let is_marker_start = false;
        let is_marker_end = false;
        for (index; index < data.length; index++) {
            if (!is_marker_start) {
                marker_type = data[index] && this.markers.find(mark => data[index].startsWith(mark));
                if (marker_type) {
                    is_marker_start = true;
                    marker_start = index;
                }
                continue;
            }
            if (!is_marker_end) {
                is_marker_end = data[index] && this.markers.find(mark => data[index].startsWith(mark));
                if (is_marker_end) {
                    marker_end = index;
                }
            }
            if (is_marker_start && is_marker_end) {
                break;
            }
        } // e

        if (is_marker_start) {
            var end = marker_end || data.length;
            var value = this.copyContent(marker_start, end, data);// get content between two markers
            let result = { type: marker_type, start: marker_start, end, value } // check
            return result;
        } else {
            return null;
        }


    }

    //copy content between two markers
    copyContent(marker_start, marker_end, data) {
        var content = "";
        for (var i = marker_start + 1; i < marker_end; i++) {
            content += data[i] + ' ';
        }
        return content;
    }
}

var bar = new fileParser(['N1', 'LIVRE', 'TITRE', 'CHAPITRE', 'SECTION', 'Paragraphe', 'Article', 'DMY']);

fs.readFile('./sample.txt', 'utf8', async function (err, data) {
    if (err) {
        return console.log(err);
    }
    console.log("start")
    // var data = data.toString().split('\n');
    var data = data.replace(/\r\n/g, "\n").split('\n');
    var test1 = await bar.parser(undefined,[], data);
    console.log("end")
    fs.writeFile('doc2.json', JSON.stringify(test1), function (err) {
        if (err) {
            return console.error(err);
        }
        // bucket.insert('code::2156', JSON.stringify(test1), function (err, result) {
        //     if (err) {
        //       console.log(err)
        //     } else {
        //       console.log(result)
        //     }
        //   })

    });
});

const server = app.listen(3000, function () {
    console.log("Listening on port %s...", server.address().port);
});



app.get('/', function (req, resp) {
    resp.render('home');
});

app.post('/search', function (req, res) {
    var search_string = req.body.nom;
    const SearchQuery = couchbase.SearchQuery;
    const resu = {};
    const query = SearchQuery.new("articles", SearchQuery.match(search_string));
    
    bucket.query(query, function (error, result, meta) {
        if (error) {
            return console.log("ERROR: ", error);
        }
        
        res.render('search', { result: result });
        
    });
});






// app.get("/code/:id", function(req, res) {
//     bucket.get(req.params.id, function(error, result) {
//         if(error) {
//             return res.status(400).send(error);
//         }
//         console.log(result);
//         res.send(result);
//     });
// });






